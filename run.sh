#!/bin/bash

# export PATH="$PWD/bin:$PATH"

here=$(dirname $(realpath $0))
export PYTHONPATH="$(dirname $here)/python:$PYTHONPATH"
export PATH="$here:$PATH"

NODOWNLOAD=
NOPARSE=

opts="$(getopt -o 'h?' -l nodownload,noparse,help -- "$@")"
set -- $opts

print_help() {
	cat <<- EOF
	Usage: $0 
	--nodownload : skip download step
	--noparse : skip html->json step
	EOF
}

while [ $# -gt 1 ]; do
	case $1 in
		--help|-h|-?)
			print_help
			exit 0 ;;
		--nodownload) NODOWNLOAD=1; shift ;;
		--noparse)    NOPARSE=1;    shift ;;
		--) break ;;
		*)
			echo "Unknown argumnet '$1', ignoring"
			shift
	esac
done

run=$(date)
date=$(date -I)

die() {
	echo "$@"
	exit 1
}

echo $PATH

for prgr in parse-index parse-course parse-program insert-course.py \
	insert-program.py static-generator.py makeman
do
	which $prgr >/dev/null || die "'$prgr' missing from path"
done



# export TMPDIR=cache/$date
# export LOGDIR=log
# export ARTDIR=artefacts/$date

# see systemd.exec(5)
export TMPDIR=${RUNTIME_DIRECTORY:-$PWD}/cache/$date
export ARTDIR=${RUNTIME_DIRECTORY:-$PWD}/artefacts/$date
export LOGDIR=${LOGS_DIRECTORY:-$PWD/log}
export run
export date

mkdir -p "$TMPDIR"
mkdir -p "$TMPDIR"/{course,program}-html
mkdir -p "$ARTDIR"/{course,program}-json
mkdir -p $LOGDIR

if [ "x$NODOWNLOAD" = "x" ]; then
	set -x

	# Build parse-index and parse-course
	wget \
		--append-output="$LOGDIR/$date-wget-log" \
		--output-document="$TMPDIR/all-courses.html" \
		"https://liu.se/studieinfo/?type=Course&term=&take=10000"

	wget \
		--output-document="$TMPDIR/all-programs.html" \
		--append-output="$LOGDIR/$date-wget-log" \
		"https://liu.se/studieinfo/?type=Program&term=&take=10000"


	parse-index "$TMPDIR/all-courses.html"  "$TMPDIR/course-download.sh"
	parse-index "$TMPDIR/all-programs.html" "$TMPDIR/program-download.sh"

	"$TMPDIR/course-download.sh" "$TMPDIR/course-html"
	"$TMPDIR/program-download.sh" "$TMPDIR/program-html"
fi

set -x

# parse courses

if [ "x$NOPARSE" = "x" ]; then
	parse-course --indir "$TMPDIR/course-html/" --outdir "$ARTDIR/course-json/"

	# parse programs

	set +x

	for f in $TMPDIR/program-html/*; do
		echo "parse-program $(basename $f .html)"
		parse-program \
			"$f" "$ARTDIR/program-json/$(basename -s '.html' "$f").json"
	done
fi

set -x

# rebuild db

db=$ARTDIR/studieinfo.db

rm "$db"
# TODO path to schema file /usr/share/studieinfo/schema.sql
sqlite3 "$db" < /usr/share/studieinfo/schema.sql
insert-program.py --database "$db" --indir "$ARTDIR/program-json"
insert-course.py --database "$db"  --indir "$ARTDIR/course-json"

# TODO this doesn't genererate kurssidor?
static-generator.py \
	--database "$db" \
	--outdir "$ARTDIR/html" \
	--templates /usr/share/studieinfo/templates
# TODO template path


mkdir -p "$ARTDIR/man7"
makeman --outdir "$ARTDIR/man7" --database "$ARTDIR/studieinfo.db"

set +x

# compress it!
for f in "$ARTDIR"/man7/*; do
	echo "gzip $(basename $f)"
	gzip "$f"
done
# tar --create --file man7.tar man7

echo Run finished
echo Start: $run
echo End:   $(date)

mkdir -p $TMPDIR/deb-pkg/DEBIAN/
mkdir -p $TMPDIR/deb-pkg/usr/share/man/man7/

rsync -a $ARTDIR/man7 $TMPDIR/deb-pkg/usr/share/man/man7

cat > $TMPDIR/deb-pkg/DEBIAN/control << EOF
Package: studieinfo
Version: $date
Architecture: all
Essential: no
Section: doc
Priority: optional
Maintainer: Hugo Hörnquist <hugo@lysator.liu.se>
Installed-Size: $(du -s $TMPDIR/deb-pkg/usr | cut -f1)
Description: Studieinfo som man-sidor.
EOF

dpkg -b $TMPDIR/deb-pkg $ARTDIR/studieinfo.deb

set -x

if [ "x$SYSTEM_UNIT" != "x" ]; then
	install.sh
fi
