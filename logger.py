import os
import logging
import logging.config

import yaml

def setup_logging(
        path='/etc/studieinfo/logging.yaml',
        default_level=logging.INFO,
        env_key='LOG_CONFIG'
        ):

    path = os.getenv(env_key, path)

    if os.path.exists(path):
        with open(path) as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

