.PHONY: all install clean

PREFIX := /usr
DESTDIR := /

ALL = parse-index parse-course parse-program.pppy makeman/makeman
all: $(ALL)

clean:
	-rm $(ALL)
	$(MAKE) -C makeman clean
	$(MAKE) -C www clean

install: all
	$(MAKE) -C makeman DESTDIR=$(DESTDIR) PREFIX=$(PREFIX) install
	$(MAKE) -C www     DESTDIR=$(DESTDIR) PREFIX=$(PREFIX) install
	install -d $(DESTDIR)/$(PREFIX)/lib/systemd/system
	install -m 644 -t $(DESTDIR)/$(PREFIX)/lib/systemd/system system/studieinfo-*
	install -d $(DESTDIR)/$(PREFIX)/lib/studieinfo/bin
	install -t $(DESTDIR)/$(PREFIX)/lib/studieinfo/bin \
		insert-course.py insert-program.py \
		parse-course parse-index \
		parse-program \
		run.sh install.sh
	install -m 644 -t \
		$(DESTDIR)/$(PREFIX)/lib/studieinfo/bin \
		parse-program.pppy
	install -m 644 -t $(DESTDIR)/$(PREFIX)/share/studieinfo \
		schema.sql
	install -d $(DESTDIR)/$(PREFIX)/lib/studieinfo/python
	install -m 644 -t $(DESTDIR)/$(PREFIX)/lib/studieinfo/python \
		logger.py
	install -d $(DESTDIR)/etc/studieinfo
	install -m 644 -t $(DESTDIR)/etc/studieinfo/ \
		logging.yaml
	-rm $(DESTDIR)/$(PREFIX)/share/studieinfo/www/kurs
	-rm $(DESTDIR)/$(PREFIX)/share/studieinfo/www/program
	-rm $(DESTDIR)/$(PREFIX)/share/studieinfo/www/kurs-html
	-rm $(DESTDIR)/$(PREFIX)/share/studieinfo/www/program-html
	ln -s /var/lib/studieinfo/kurs            $(DESTDIR)/$(PREFIX)/share/studieinfo/www
	ln -s /var/lib/studieinfo/program         $(DESTDIR)/$(PREFIX)/share/studieinfo/www
	ln -s /var/lib/studieinfo/stuff           $(DESTDIR)/$(PREFIX)/share/studieinfo/www/stuff


%.pppy: %.py
	cpp $< > $@

parse-index: parse-index.scm
	csc $< -o $@

parse-course: parse-course.scm
	csc -d2 $< -o $@

makeman/makeman:
	$(MAKE) -C makeman
