;;; -*- geiser-scheme-implementation: 'chicken -*-
;;; Commentary:
;;; Parses course HTML pages from LiU's studieinfo,
;;; and produces (semi) normalized json files
;;; ((hopefully) containing the same information).
;;; Code:

(import (only (chicken string) string-translate* string-split)
        (only (chicken process-context)
              command-line-arguments)
        (only (chicken pathname) pathname-file)
        (only (chicken port) with-output-to-string)
        (chicken file)
        (only (chicken file posix) file-modification-time)
        (only (chicken time posix) time->string seconds->utc-time)
        (chicken format)
        (chicken pretty-print)
        (only (chicken condition) abort condition-case)
        (srfi 1)

        (rename (only (srfi 13)
                      string-trim-both
                      string-null?
                      string-downcase
                      string-join
                      string-concatenate)
                (string-concatenate cat))

        sxml-serializer
        sxpath

        matchable

        (except html-parser html->sxml)


        (chicken process)
)

(define unspecified (begin))
(define (unspecified? v)
  (eq? unspecified v))


#|
 | åäö i början (samt slutet?) av ord dödar mellanslag.
 |#

;; Borrowed from html->sxml documentation, but with 'text replaced
;; with stripping version.
(define html->sxml
  (let ((parse
          (make-html-parser
            'start: (lambda (tag attrs seed virtual?) '())
            'end:   (lambda (tag attrs parent-seed seed virtual?)
                      `((,tag ,@(if (pair? attrs)
                                    `((@ ,@attrs) ,@(reverse seed))
                                    ;; string-append possibly possible here.
                                    (reverse seed)))
                        ,@parent-seed))
            'decl:    (lambda (tag attrs seed) `((*DECL* ,tag ,@attrs) ,@seed))
            'process: (lambda (attrs seed) `((*PI* ,@attrs) ,@seed))
            'comment: (lambda (text seed) `((*COMMENT* ,text) ,@seed))
            'text:    (lambda (text seed) (let ((str (string-trim-both text)))
                                            (if (string-null? str)
                                                seed
                                                (cons (string-translate*
                                                       str
                                                       '(("&ouml;" . "ö")
                                                         ("&Ouml;" . "Ö")
                                                         ("&aring;" . "å")
                                                         ("&Aring;" . "Å")
                                                         ("&auml;" . "ä")
                                                         ("&Auml;" . "Ä")))
                                                      seed))))
            )))
    (lambda o (reverse (apply parse '() o)))))

(define-syntax string-case
  (syntax-rules (else)
    [(_ key) (begin)]

    [(_ key (else body ...))
     (begin body ...)]

    [(_ key ((strings ...) body ...) rest ...)
     (if (member key (list strings ...))
         (begin body ...)
         (string-case key rest ...))]

    [(_ key (string body ...) rest ...)
     (if (string=? key string)
         (begin body ...)
         (string-case key rest ...))]))


#|
 | Ämnesområde finns bara när det finns
 | kurstidertabell => ges-for
 | senaste
 |
 | Min lägger till:
 | kursplan
 | literatur-page
 |#

(define (change-strings-to-python str)
  (string-case
   str
   ["Kurshemsida och andra länkar" "lankar"]
   ["Studierektor eller motsvarande" "studierektor"]
   ["Undervisnings- och arbetsformer" "undervisnings_och_arbetsformer"]
   [else (string-translate* (string-downcase str)
                            '(("å" . "a") ("Å" . "a")
                              ("ä" . "a") ("Ä" . "a")
                              ("ö" . "o") ("Ö" . "o")
                              (" " . "_")
                              ))]

   ))

(define (print-pair pair port)
  (display "\"" port)
  (display (change-strings-to-python (car pair)) port)
  (display "\": " port)
  (sxml->json (cdr pair) port))

(define (sxml->json sxml port)
  (cond [(list? sxml)
         (display "{" port)
         (unless (null? sxml)
           (let ((sxml (remove unspecified? sxml)))
             (print-pair (car sxml) port)
             (for-each (lambda (pair) (display ", " port)
                          (print-pair pair port))
                       (cdr sxml))))
         (display "}" port)
         (newline port)]
        [(string? sxml)
         (write sxml port)]
        [(boolean? sxml)
         (display (if sxml "true" "false") port)]
        [(vector? sxml)
         (display "[" port)
         (for-each (lambda (i v)
                     (unless (= i 0)
                       (display ", " port))
                     (sxml->json v port))
                   (iota (vector-length sxml))
                   (vector->list sxml))
         (display "]" port)
         (newline port)
         ]))



;; Used for parsing tables
(define (row-col-widths tr)
  (map (lambda (td)
         (cons td
               (cond (((if-car-sxpath '(@ colspan "text()")) td)
                      => string->number)
                     (else 1))))
       (cdr tr)))

(define (generate-table-schema table-row)
  (filter car
          (map (lambda (th)
                 (cons (cond (((if-sxpath "text()") th)
                              => (o string-downcase cat))
                             (else #f))
                       (cond (((if-car-sxpath '(@ colspan "text()")) th)
                              => string->number)
                             (else 1))))
               table-row)))

(define (make-link-sexp a)
  `(("link" . ,(cat ((sxpath '(@ href "text()")) a)))
    ("text" . ,(cat ((sxpath "text()") a)))))

(define (parse-html-table schema rows)
  (list->vector
   (map (lambda (tr)
          (let loop ((headers schema)
                     (fields (row-col-widths tr))
                     (width 0)
                     (part '()))
            (if (null? headers) '()
                (if (= (+ width (cdar fields))
                       (cdar headers))
                    ;; we have a field
                    (cons (cons (caar headers)
                                ((lambda (lst)
                                   (if (= 1 (length lst))
                                       (car lst)
                                       (list->vector lst)))
                                 (map (lambda (td)
                                        (cond [((if-car-sxpath '(a)) td) => make-link-sexp]
                                              [else (cat ((sxpath '(// "text()")) td))]))
                                      (reverse (cons (caar fields) part)))))
                          (loop (cdr headers)
                                (cdr fields)
                                0 '()))
                    ;; we need more columns for our field
                    (loop headers (cdr fields)
                          (+ width (cdar fields))
                          (cons (caar fields) part))))))
        rows)))





;; generella bestämeleser bör vara samma för samtliga kurser, spara endast en gång
;; https://liu.se/studieinfo/kurs/950a07/ht-2019
(define (course->json input-file)

  (define doc
    (call-with-input-file input-file html->sxml))

  (cond [(null? doc)
         (display "Empty document\n" (current-error-port))]
        [(string=? "400"
                   (substring ((car-sxpath '(// title "text()")) doc)
                              0 3))
         (display "400 page\n" (current-error-port))]

        [(string=? "https://liu.se/e404-sida"
                   ((car-sxpath '(// @ data-itemurl "text()")) doc))
         (display "404-page\n" (current-error-port))]
        [else
         (let ((object
                `(

                  ("stat_mtime" . ,(time->string
                                     (seconds->utc-time
                                       (file-modification-time input-file))
                                     "%Y-%m-%d %H:%M:%S"))

                  ("senaste" . "1")

                  ;; Tidigare upstream-url
                  ("url" . ,(cat ((sxpath '(// @ data-itemurl "text()")) doc)))

                  ,@(map (lambda (a)
                           (cons (cat ((sxpath '("text()")) a))
                                 (cat ((sxpath '(@ href "text()")) a))))
                         ((sxpath '(// aside (div (h3 (equal? "Ladda ner")))
                                       // a))
                          doc))

                  ,@(let ((kurstider ((car-sxpath '(// (select (@ name (equal? "Kursstart")))))
                                      doc)))

                      (append 
                        `(("annos" .
                           ,(list->vector (map (lambda (link str)
                                                 `(("link" . ,link)
                                                   ("text" . ,str)))
                                               ((sxpath '(option @ value "text()")) kurstider)
                                               ((sxpath '(option "text()")) kurstider)))))

                        ;; ((sxpath '((option (@ (selected)) "text()")) base)
                        (match (string-split ((car-sxpath "option[@selected]/text()")
                                              kurstider))
                               [(termin year . extra)
                                ;; extra kan tydligen inehålla
                                ;; bokstaven 'A' eller 'B'...
                                `(("anno" . ,year)
                                  ("termin" . ,termin))])))

                  ,@(let ((side-info ((sxpath '(// (aside (@ class (equal? "studyguide-aside")))
                                                   (div ((equal? (h3 "Information"))))
                                                   div))
                                      doc)))
                      (append
                       (match
                           (map cat
                                (map (sxpath "text()")
                                     ((sxpath '(h4)) side-info)))
                         [(namn-sv hp namn-en)
                          `(("kursnamn_sv" . ,namn-sv)
                            ("kursnamn_en" . ,namn-en)
                            ("hp" . ,(car (string-split hp))))])
                       (map (lambda (p)
                              (cons (cat ((sxpath '(span "text()")) p))
                                    (cat ((sxpath '("text()")) p))))
                            ((sxpath '(p)) side-info))))

                  ;; examinationsmoment har egen sida, men finns även under "kursplan/examination",
                  ;; fast inte alltid. Det här är för egna sidan, och verkar fungera bättre
                  ("examinationsmomenttabell"
                   . ,(let ((div ((car-sxpath '(// (div (@ class (equal? "tab-content"))) "div[4]"))
                                  doc)))
                        ;; NOTE python code different
                        `(("table" . ,(parse-html-table '(("kod" . 1) ("beskrivning" . 1)
                                                          ("betygsskala" . 1) ("hp" . 1))
                                                        ((sxpath '(div table tr)) div)))

                          ,(let ((str (cat ((sxpath '(div div "text()")) div))))
                             (unless (string-null? str)
                               `("rest" . ,str))))))


                  ,@(map (lambda (div)
                           (let ((key (cat ((sxpath '((h3 (@ class (equal? "title"))) "text()")) div))))

                             (string-case key
                               ;; We filter out these since they are given on a
                               ;; better form elsewhere in the document
                               [("Examination" "Kursen ges för")]
                               [else
                                (cons key
                                      (string-join
                                       (map (cut serialize-sxml <> method: 'html)
                                            (let f ((body ((sxpath '((*not* h3))) div)))
                                              (filter-map (lambda (t)
                                                            (cond ((string? t) (string-trim-both t))
                                                                  ((list? t) 
                                                                   (if ((if-sxpath '(@ href (equal? ""))) t)
                                                                     #f (f t)))
                                                                  (else t)))
                                                          body)))))])))
                         ((sxpath '(// (section (@ class (equal? "studyguide-block")))
                                       (div)))
                          doc))

                  ,(cond [((if-sxpath
                             '(// (div (@ class (equal? "tab-content"))) "div[6]" // table // tr))
                            doc) => (lambda (table)
                                      (let ((table-schema (generate-table-schema (car table))))
                                        `("övriga-dokument" . ,(parse-html-table table-schema (cdr table)))))])


                  ;; kursliteratursidan
                  ;; null | false | {?"bocker", ?"ovrigt", ?"kompendier", ?"download"}
                  ("litteratur_page"
                   . ,(let ((base ((sxpath
                                    ;; (@ class (equal? "studyguide-block course-literature")))
                                    '(// (div (@ class (equal? "tab-content")))
                                         "div[3]" div))
                                   doc)))
                        (match base
                          [(info download)
                           ;; https://liu.se/studieinfo/kurs/tddd71/ht-2017
                           ;; TODO alerts on pages
                           `(,@(map (lambda (div)
                                      (cons
                                        ;; NOTE This can be empty
                                        ;; TODO filter
                                       (cat ((sxpath '(h3 "text()")) div))
                                       ;; this kills the formatting tags
                                       (cat ((sxpath '(div // "text()")) div))))
                                    ((sxpath '(div)) info))
                             ("download" .
                              ,(cond [((if-car-sxpath '(// a)) download)
                                      => make-link-sexp])))]
                          [_ #f]))
                   )

                  ,(let* ((table ((sxpath
                                   '(// (div (@ class (equal? "tab-content")))
                                        "div[1]" // table // tr))
                                  doc))
                          (table-schema
                           (generate-table-schema (cdar table))))
                     `("ges-för"
                       .  ,(parse-html-table table-schema (cdr table))))


                  ;; Huvudområde, Nivå, Kurstyp, Examinator, 'Studierektor eller motsvarande'
                  ;; ,@(map (lambda (row)
                  ;;          (cons (string-downcase (cat ((sxpath '(h3 "text()")) row)))
                  ;;                (cat ((sxpath '("text()")) row))))

                  ;;        ((sxpath '(// (div (@ class (equal? "studyguide-block-full-width")))
                  ;;                      (div (@ class (equal? "row")))
                  ;;                      // div))
                  ;;         doc))
                  )))


           (sxml->json object (current-output-port)))]))

;; TODO kontaktinfo

; (define (multicore-main thread-count files)
;   (define pids
;     (map (lambda (part)
;            (process-fork
;              (lambda ()
;                (define i 0)
;                (for-each (lambda (f)
;                            (set! i (+ i 1))
;                            (format #t "~a Parsing ~a~%" i f)
;                            (with-output-to-file
;                              (format #f "cache/course-json/~a.json" (pathname-file f))
;                              (lambda () (course->json f))))
;                          part))))
;          (chop files (floor (/ (length files)
;                                thread-count)))))
; 
;   (for-each (lambda (pid) (nth-value 1 (process-wait pid))) pids)
;   (print "Everything done\n"))

(define (single-core-main indir outdir)
  (let ((i 0))
    (for-each (lambda (f)
                (set! i (+ 1 i))
                (format #t "~a Parsing ~a~%" i f)

                (condition-case
                  (with-output-to-file (format #f "~a/~a.json" outdir (pathname-file f))
                    (lambda () (course->json (string-append indir "/" f))))
                  (err ()
                       (format #t "Something went wrong with ~a, ~a~%" f err))))
              (directory indir #f)
              )))




;; check if main
(unless (null? (command-line-arguments))

  (let ((opts
          (let loop ((rem (command-line-arguments)))
            (cond ((null? rem) '())
                  ((string=? "--indir" (car rem))
                   (cons (cons 'indir (cadr rem))
                         (loop (cddr rem))))
                  ((string=? "--outdir" (car rem))
                   (cons (cons 'outdir (cadr rem))
                         (loop (cddr rem))))
                  (else (loop (cdr rem)))
                  ))))

    (unless (and (assoc 'indir opts) (assoc 'outdir opts))
      (display "Usage: ./parse-course --indir <dir> --outdir <dir>\n")
      (format #t "Opts: ~a~%" opts)
      (abort 1))

    (single-core-main
      (cdr (assoc 'indir opts))
      (cdr (assoc 'outdir opts)))))
