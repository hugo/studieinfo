#!/usr/bin/env python3

from jinja2 import Environment, FileSystemLoader
import sqlite3
import os
import json
from argparse import ArgumentParser
import logging
import traceback

parser = ArgumentParser(description='Generate static HTML files')
parser.add_argument('--database',
        help='sqlite3 input file')
parser.add_argument('--outdir')
parser.add_argument('--templates')
parser.add_argument('--file')

args = parser.parse_args()
outdir = args.outdir or '.'

template_dir = args.templates or os.path.dirname(os.path.abspath(__file__)) + "/templates"

log = logging.getLogger("static-generator")

# Load templates
env = Environment(loader=FileSystemLoader(template_dir))

os.makedirs(outdir, exist_ok=True)


def generate_program_pages(db):
    c = db.cursor()
    c.execute("""
            SELECT * FROM program
            WHERE programnamn_sv IS NOT NULL
              AND programkod IS NOT NULL""")  # ORDER BY antagningstillfalle")
    programs = c.fetchall()
    try:
      template = env.get_template('program.html')
      for tillfalle in programs:
        os.makedirs(f"{outdir}/program/{tillfalle['programkod']}", exist_ok=True)

        c.execute("SELECT * FROM program WHERE programkod = ?", (tillfalle['programkod'],))
        varianter = c.fetchall()

        programplan = json.loads(tillfalle['programplan_json'])
        template.stream(program=tillfalle, varianter=varianter, programplan=programplan) \
                .dump(f"{outdir}/program/{tillfalle['programkod']}.html")
        template.stream(program=tillfalle, varianter=varianter, programplan=programplan) \
                .dump(f"{outdir}/program/{tillfalle['programkod']}/index.html")
        template.stream(program=tillfalle, varianter=varianter, programplan=programplan) \
                .dump(f"{outdir}/program/{tillfalle['programkod']}/{tillfalle['antagningskod']}.html")

    except Exception as e:
        log.fatal("Rendering of %s failed with exception %s",
                  tillfalle['programkod'], e)
        log.debug("traceback: %s", traceback.format_exc())



def generate_course_pages(db):
    c = db.cursor()
    c.execute("SELECT kurskod FROM kurs")
    results = c.fetchall()
    codes = [result['kurskod'] for result in results]

    try:
      template = env.get_template(f'kurs.html')
      os.makedirs(f"{outdir}/kurs/", exist_ok=True)
      for code in codes:
        c.execute("SELECT * FROM kurs WHERE kurskod = ?", (code,))
        row = c.fetchone()
        c.execute("SELECT * FROM kurstider WHERE kurs_id = ?", (row['id'],))
        kurstider = c.fetchall()
        sprak = [kurstid['sprak'] for kurstid in kurstider]
        sprak = ["/".join(s[:2] for s in sp.split("/")) for sp in sprak]
        c.execute("SELECT * FROM examinationsmoment WHERE kurs_id = ?", (row['id'],))
        examinationsmoment = c.fetchall()
        template.stream(kurs=row, kurstider=kurstider, examinationsmoment=examinationsmoment, sprak=sprak) \
                .dump(f"{outdir}/kurs/{code}.html")

    except Exception as e:
        log.fatal("While rendering kurs %s the following error occured: %s",
                code, e)
        log.debug(traceback.format_exc())


if args.file:
    template = env.get_template(args.file)
    template.stream().dump(f"{outdir}/{args.file}")

    """
    template = env.get_template(f'about.html')
    template.stream().dump(f"{outdir}/about.html")

    template = env.get_template(f'index.html')
    template.stream().dump(f"{outdir}/index.html")
    """

else:
    # Get db
    # db = sqlite3.connect(f"file:{args.database}?mode=ro", uri=True)
    db = sqlite3.connect(args.database)
    db.row_factory = sqlite3.Row
    try:
        generate_program_pages(db)
        generate_course_pages(db)
    finally:
        db.commit()
        db.close()
