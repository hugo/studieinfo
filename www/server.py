from flask import Flask, request, g, render_template
from os import getenv
app = Flask(__name__)

from jinja2 import Environment, FileSystemLoader
import sqlite3
import json

def connect_db():
    dbpath = getenv('DBPATH', 'studieinfo.db')
    cold_db = sqlite3.connect(dbpath)
    cold_db.row_factory = sqlite3.Row
    return cold_db

def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        g.db = connect_db()
        db = g.db
    return db

@app.route("/")
def root():
    c = get_db().cursor()
    c.execute("SELECT programkod FROM program WHERE programnamn_sv IS NOT NULL ORDER BY programkod ")
    results = c.fetchall()
    programs = [result['programkod'] for result in results]
    return render_template("index.html", programs=programs)

@app.route("/search/", methods=['GET'])
def simple_search():
    template_path = getenv('TEMPLATE_PATH', 'templates')
    env = Environment(loader=FileSystemLoader(template_path))
    template = env.get_template('results.html')
    query = request.args.get('q')
    if query:
        query = "%" + query + "%"

    try:
        c = get_db().cursor()
        if query:
            c.execute("SELECT * FROM kurs WHERE kurskod LIKE ? OR kursnamn_sv LIKE ? AND kursnamn_sv IS NOT NULL", (query,query))
        else:
            c.execute("SELECT * FROM kurs WHERE kursnamn_sv IS NOT NULL")
        kurs_rows = c.fetchall()

        if query:
            c.execute("SELECT * from program WHERE programkod LIKE ? OR programnamn_sv LIKE ? AND programnamn_sv IS NOT NULL", (query,query))
        else:
            c.execute("SELECT * FROM program WHERE programnamn_sv IS NOT NULL")
        prog_rows = c.fetchall()

        res = template.render(kurs_rows=kurs_rows, kurslen=len(kurs_rows),
                prog_rows=prog_rows, proglen=len(prog_rows),
                query = query.replace("%","").replace("AND",""))
    except:
        import traceback
        tb = traceback.format_exc()
        return "nej. internt fel.\n"  + tb


    return res


@app.route("/kurs/<name>", methods=['GET'])
def kurs(name):
    c = get_db().cursor()
    c.execute("SELECT * FROM kurs WHERE kurskod = ?", (name.upper(),))
    row = c.fetchone()
    if row:
      c.execute("SELECT * FROM kurstider WHERE kurs_id = ?", (row['id'],))
      kurstider = c.fetchall()
      c.execute("SELECT * FROM examinationsmoment WHERE kurs_id = ?", (row['id'],))
      examinationsmoment = c.fetchall()
      sprak = [kurstid['sprak'] for kurstid in kurstider]
      sprak = ["/".join(s[:2] for s in sp.split("/")) for sp in sprak]
      return render_template("kurs.html", kurs=row, kurstider=kurstider, examinationsmoment=examinationsmoment, sprak=sprak)
    else:
      return render_template("404.html")


@app.route("/about", methods=['GET'])
def about():
    return render_template("about.html")

@app.route("/program/<name>/<kod>", methods=['GET'])
def program(name, kod=None):
    c = get_db().cursor()
    if kod:
        c.execute("SELECT * FROM program WHERE programkod = ? AND antagningskod = ?", (name.lower(), kod))
    else:
        c.execute("SELECT * FROM program WHERE programkod = ?", (name.lower(),))
    row = c.fetchone()
    if row:
        c.execute("SELECT * FROM program WHERE programkod = ?", (name.lower(),))
        varianter = c.fetchall()
        programplan = json.loads(row['programplan_json'])
        return render_template("program.html", program=row, programplan=programplan, varianter=varianter)
    else:
        return render_template("404.html")

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


if __name__ == '__main__':
      app.run(debug=True, host='0.0.0.0', port=8081)
