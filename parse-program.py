/*
Parses program HTML pages from LiU's studieinfo,
and produces (semi) normalized json files
((hopefully) containing the same information).

Run through cpp to to generate actual python code.
*/

from bs4 import BeautifulSoup
import json
import traceback
import sys
import os
import logging
from logger import setup_logging

setup_logging()
log = logging.getLogger("studieinfo.parse.program")

def try_it(f):
    res = None
    try:
        res= f()
    except Exception as e:
        log.error("%s\n%s", sys.argv[1], e)
        log.debug('traceback', exc_info=e)

    return res


#define FUCK(...) try_it(lambda : __VA_ARGS__)

def get_everything(soup):
    overview = FUCK(soup.find("div", {'class':"studyguide-block-full-width"}))

    def kurs(k):
        return {
            'kurskod': FUCK(k['data-course-code']),
            'kursnamn': FUCK(k.find("a").text.strip()),
            'hp': FUCK(k.findAll("td")[2].text.strip()),
            'niva': FUCK(k.findAll("td")[3].text.strip()),
            'block': FUCK(k.findAll("td")[4].text.strip()),
            'vof': FUCK(k.findAll("td")[5].find("span").text.strip()),
            }

    def period(p):
        return {
            'period': FUCK(p.find("th").text),
            'kurser': list(map(kurs, FUCK(p.findAll("tr", {'class':"main-row"})))),
            }

    def specialisering(s):
        return {
            'specialisering': FUCK(s.find("label").text.strip().replace('\t','').replace('\n',' ')),
            'perioder': FUCK(list(map(period, s.findAll("tbody", {'class':"period"})))),
            }

    def termin(t):
        return {
            'termin': FUCK(t.find("h3").text),
            'specialiseringar': FUCK(list(map(specialisering, t.findAll("div", {'class': "specialization"})))),
            }

    return {
        'url'                    : FUCK(soup.find("head")['data-itemurl']),
        'programkod'             : FUCK(soup.find("head")['data-itemurl'].split('/')[-2]),
        'programnamn_sv'         : FUCK(soup.find("div", {"class":"main-container study-guide-main-container"}).find("header").find("h1").text.strip()),
        'programnamn_en'         : FUCK(soup.find("div", {"class":"main-container study-guide-main-container"}).find("header").find("p", {"class":"subtitle"}).text.strip()),
        'antagningskod'          : FUCK(soup.find("head")['data-itemurl'].split('/')[-1]),
        'undervisningssprak'     : FUCK(overview.find("h3", text="Undervisningsspråk").next_sibling.strip()),
        'studieort'              : FUCK(overview.find("h3", text="Studieort").next_sibling.strip()),
        'examensbenamning'       : FUCK(overview.find("h3", text="Examensbenämning").parent.find("p").text),
        'studietakt'             : FUCK(overview.find("h3", text="Studietakt").next_sibling.strip()),
        'utbildningsplan'        : FUCK(soup.find("section", {"class":"studyguide-block"}).prettify()),
        'generella_bestammelser' : FUCK(soup.find("div", {"class":"studyguide-block common-rules"}).prettify()),
        'programplan'            : FUCK(list(map(termin, soup.findAll("article", {'class':"accordion box semester js-semester is-toggled"})))),
        'antagningstillfalle'    : FUCK(soup.find("select", id="related_entity_navigation").find("option", selected=True).text.strip()),
        'antagningskoder'        : FUCK(list(map(lambda x: x['value'].split('/')[-1] ,soup.find("select", id="related_entity_navigation").findAll("option")))),
        }


if __name__ == "__main__":
    filename, out_filename, *_ = sys.argv[1:]
    try:
        j = ""
        with open(filename, 'r') as f:
            res = get_everything(BeautifulSoup(f.read(), "html5lib"))
            from datetime import datetime
            t = os.stat(f.fileno()).st_mtime
            res['stat_mtime'] = datetime.utcfromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')
            res['senaste'] = FUCK(0 if '+' in filename else 1)
            res['programplan_json'] = FUCK(json.dumps(res['programplan']))
            j = json.dumps(res)

        with open(out_filename, 'w') as f:
            f.write(j)
            

    except Exception as e:
        log.error("%s\n%s", filename, e)
        log.debug('traceback', exc_info=e)
