# Studieinfo
### Syftet med projekt studieinfo
- Tillstånd :: studieinfo = långsamt
- Önskat tillstånd :: studieinfo = snabbt
- Lösning :: studieinfo.lysator.liu.se

### Bruksanvisning
- `make` bör bygga ihop samtliga komponenter.
- `run.sh` (efter `make`) bör ladda ner och hantera all data
- `build-rpm` bör bygga ihop en rpm-fil

### Chicken Scheme Dependencies
- html-parser
- sxpath
- matchable
- sxml-serializer

## Att göra
- Hur skickas parametrar runt. Just nu är det ett mismach av miljövariabler,
  kommandoradsflaggor och konfigurationsfiler. Utan någon ordentlig tanke.
