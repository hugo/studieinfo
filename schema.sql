PRAGMA FOREIGN_KEYS = ON;
  
CREATE TABLE kurs (
  id INTEGER PRIMARY KEY NOT NULL,
  kurskod TEXT,
  url TEXT,
  termin TEXT,
  anno TEXT,
  kursnamn_sv TEXT,
  hp TEXT,
  kursnamn_en TEXT,
  kurstyp TEXT,
  fakultet TEXT,
  galler_fran TEXT,
  faststalld_av TEXT,
  faststallandedatum TEXT,
  revideringsdatum TEXT,
  diarienummer TEXT,
  huvudomrade TEXT,
  utbildningsniva TEXT,
  fordjupningsniva TEXT,
  forkunskapskrav TEXT,
  rekommenderade_forkunskaper TEXT,
  larandemal TEXT,
  kursinnehall TEXT,
  undervisnings_och_arbetsformer TEXT,
  betygsskala TEXT,
  ovrig_information TEXT,
  amnesomrade TEXT,
  institution TEXT,
  studierektor TEXT,
  examinator TEXT,
  lankar TEXT,
  undervisningstid TEXT,
  kurslitteratur TEXT,
  stat_mtime TEXT,
  senaste INTEGER NOT NULL
  );

CREATE INDEX `kurskod` ON `kurs` (
	`kurskod`	ASC
);

CREATE TABLE kurstillfallen (
  kurs_id INTEGER,
  kurskod TEXT,
  termin TEXT,
  anno TEXT,
  PRIMARY KEY (kurskod, termin, anno),
  FOREIGN KEY (kurs_id) REFERENCES kurs(id)
  );

CREATE TABLE kurstider (
  kurstid_id INTEGER PRIMARY KEY NOT NULL,
  kurs_id INTEGER,
  programkod TEXT,
  antagningskod TEXT,
  programtermin TEXT,
  termin TEXT,
  period TEXT,
  block TEXT,
  sprak TEXT,
  studieort TEXT,
  vof TEXT,
  programurl TEXT,
  programnamn TEXT,
  FOREIGN KEY (kurs_id) REFERENCES kurs(id)
  );

CREATE INDEX `kurs_id` ON `kurstider` (
	`kurs_id`	ASC
);

CREATE TABLE examinationsmoment (
  kurs_id INTEGER,
  kod TEXT,
  beskrivning TEXT,
  betygsskala TEXT,
  hp TEXT,
  PRIMARY KEY (kurs_id, kod),
  FOREIGN KEY (kurs_id) REFERENCES kurs(id)
  );

CREATE TABLE period (
  kurstid_id INTEGER,
  period TEXT,
  PRIMARY KEY (kurstid_id, period),
  FOREIGN KEY (kurstid_id) REFERENCES kurstider(kurstid_id)
  );

CREATE TABLE block (
  kurstid_id INTEGER,
  period TEXT,
  block TEXT,
  PRIMARY KEY (kurstid_id, period, block),
  FOREIGN KEY (kurstid_id, period) REFERENCES period(kurstid_id, period)
  );

CREATE TABLE program (
  id INTEGER PRIMARY KEY NOT NULL,
  url TEXT,
  programkod TEXT,
  programnamn_sv TEXT,
  programnamn_en TEXT,
  antagningskod TEXT,
  undervisningssprak TEXT,
  studieort TEXT,
  examensbenamning TEXT,
  studietakt TEXT,
  utbildningsplan TEXT,
  generella_bestammelser TEXT,
  antagningstillfalle TEXT,
  programplan_json TEXT,
  stat_mtime TEXT,
  senaste INTEGER
  );

CREATE TABLE programplan_termin (
  id INTEGER PRIMARY KEY NOT NULL,
  program_id INTEGER,
  termin TEXT,
  FOREIGN KEY (program_id) REFERENCES program(id)
  );

CREATE TABLE programplan_specialisering (
  id INTEGER PRIMARY KEY NOT NULL,
  termin_id INTEGER,
  specialisering TEXT,
  FOREIGN KEY (termin_id) REFERENCES programplan_termin(id)
  );

CREATE TABLE programplan_period (
  id INTEGER PRIMARY KEY NOT NULL,
  specialisering_id INTEGER,
  period TEXT,
  FOREIGN KEY (specialisering_id) REFERENCES programplan_specialisering(id)
  );

CREATE TABLE programplan_kurs (
  id INTEGER PRIMARY KEY NOT NULL,
  period_id INTEGER,
  kurskod TEXT,
  kursnamn TEXT,
  hp TEXT,
  niva TEXT,
  block TEXT,
  vof TEXT,
  FOREIGN KEY (period_id) REFERENCES programplan_period(id)
  );
