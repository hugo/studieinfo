#!/usr/bin/env python3

"""
Inserts json blobs produced by parse-course into the database.
"""

import sqlite3
import json
import sys
import os
import logging
from logger import setup_logging
from datetime import datetime
import argparse

# arg parser
# ==========

parser = argparse.ArgumentParser(description='Insert data into the database')
parser.add_argument('--database',  default='studieinfo.db', help='sqlite3 file')
parser.add_argument('--indir',  default='course-json', help='Directory of json files')

args = parser.parse_args()
db = args.database
indir = args.indir

# ====================

setup_logging()
log = logging.getLogger("studieinfo.insert.course")

log.info("Start insert")

conn = sqlite3.connect(db)
c = conn.cursor()
c.execute('PRAGMA FOREIGN_KEYS = ON')

try:
  for filename in os.listdir(indir):
    if filename[-4:] != "json":
        log.warning("Skping non-json file %s", filename)
        continue
    data = None
    log.info("Processing %s", filename)
    with open(indir+'/'+filename, 'r') as f:
        try:
            data = json.loads(f.read())
        except Exception as e:
            log.warning("empty file, or sometething")
            log.debug('traceback', exc_info=e)
            continue

    column_names = ["kurskod",
                    "url", # itemurl
                    "termin",
                    "anno",
                    "kursnamn_sv",
                    "hp",
                    "kursnamn_en",
                    "kurstyp",
                    "fakultet",
                    "galler_fran",
                    "faststalld_av",
                    "faststallandedatum",
                    "revideringsdatum",
                    "diarienummer",
                    "huvudomrade",
                    "utbildningsniva",
                    "fordjupningsniva",
                    "forkunskapskrav",
                    "rekommenderade_forkunskaper",
                    "larandemal",
                    "kursinnehall",
                    "undervisnings_och_arbetsformer",
                    "betygsskala",
                    "ovrig_information",
                    "amnesomrade",
                    "institution",
                    "studierektor",
                    "examinator",
                    "lankar",
                    "undervisningstid",
                    "kurslitteratur",
                    "stat_mtime",
                    "senaste"
                    ]

    row = tuple(data.get(x) for x in column_names)

    # if not (data['kurskod'] and data['galler_fran']):
    #     exit (0)

    try:
      c.execute('INSERT INTO kurs'
              + str(tuple(column_names))
              + 'VALUES (?'
              + (',?' * (len(column_names) - 1))
              + ')',
              row)

      c.execute('SELECT id FROM kurs WHERE kurskod=? AND anno=? AND termin=?',
                (data['kurskod'], data['anno'], data['termin']))
      kurs_id = c.fetchone()[0]

    # TODO replace this with annos
      try:
        c.execute('INSERT INTO kurstillfallen (kurs_id, kurskod, termin, anno) VALUES (?, ?, ?, ?)', (kurs_id, data['kurskod'], data['termin'], data['anno']))
      except:
        pass

# LITERATURSIDA
# TODO
# Currently the corresponding db table is missing
# Also, putting data with weak schemas in SQL is hard
#
      # try:
      #   page = data.get('littetratur_page')
      #   if page:
      #       c.execute('''INSERT INTO kurslitteratur
      #               (kurs_id, bocker, ovrigt, link_url, link_text)
      #               VALUES (?, ?, ?, ?, ?)''',
      #               (
      #                   kurs_id,
      #                   page.get("bocker"),
      #                   page.get("ovrigt"),
      #                   page.get("download").get("link"),
      #                   page.get("download").get("text")))

      # except Exception as e:
      #   print(e)
      #   with open("insert.log", "a") as f:
      #       import traceback
      #       tb = traceback.format_exc()
      #       f.write("=======================\n")
      #       f.write(filename + " :\n")
      #       f.write(str(e) + "\n")
      #       f.write("-----------------------\n")
      #       f.write(tb)

    

# EXAMINATIONSMOMENT
      try:

        rest_data = data['examinationsmomenttabell'].get('rest')
        if rest_data:
            log.warning("WARNING: REST DATA:")
            c.execute('INSERT INTO examinationsmoment (kurs_id, kod, beskrivning) VALUES (:id, "", :desc)',
                    {
                        "id": kurs_id,
                        "desc": rest_data
                    })

        for moment in data['examinationsmomenttabell']['table']:
          column_names = ["kurs_id",
                          "kod",
                          "beskrivning",
                          "betygsskala",
                          "hp",
                          ]
          moment['kurs_id'] = kurs_id
          row = tuple(moment[x] for x in column_names)
          c.execute('INSERT INTO examinationsmoment' + str(tuple(column_names)) + 'VALUES (?,?,?,?,?)', row)
      except Exception as e:
          log.warning('filename')
          log.debug('traceback', exc_info=e)

# GES FÖR TODO
      for prog in data['ges-for']:
          if type(prog['kursen_ges_for']) == str:
              log.warning("Fristående kurs...")
              continue

        # gammalt namn     => nytt namn
        # =============================
        # programkod       => kursen_ges_for[0]
        # antagningskod    => kursen_ges_for[1]["link"].split("/")[-1]
        # programtermin    => termin.split(" ")[0]
        # termin           => termin.split(" ")[1] (and trim parenthesis)
        # period           => period
        # block            => block
        # periodtabell ...
        # sprak            => sprak
        # sutdieort        => ort
        # vof              => vof
        # programurl       => kursen_ges_for[1]["link"]
        # programnamn      => kursen_ges_for[1]["text"]

          column_names = ["kurs_id",
                          "programkod",
                          "antagningskod",
                          "programtermin",
                          "termin",
                          "period",
                          "block",
                          "sprak",
                          "studieort",
                          "vof",
                          "programurl",
                          "programnamn",
                          ]
          # prog['kurs_id'] = kurs_id
          # row = tuple(prog[x] for x in column_names)

          query = ('INSERT INTO kurstider ('
                  + "\n\t, ".join(column_names)
                  + ') VALUES ('
                  + "\n\t, ".join(':' + x for x in column_names)
                  + ')')


          ges_for = prog["kursen_ges_for"][1]
          if type(ges_for) == str: # fan ta dig
              antagningskod = None
              programurl    = None
              programnamn   = None
          else:
              antagningskod = ges_for["link"].split("/")[-1]
              programurl    = ges_for["link"]
              programnamn   = ges_for["text"]

          c.execute(query, {
                      "kurs_id"       : kurs_id,
                      "programkod"    : prog["kursen_ges_for"][0],
                      "antagningskod" : antagningskod,
                      "programtermin" : prog["termin"].split(" ")[0],
                      "termin"        : " ".join(prog["termin"].split(" ")[1:]), # TODO trim parenthesis
                      # parse-course.scm leaves no guarantees here
                      "period"        : prog.get("period"),
                      "block"         : prog.get("block"),
                      "sprak"         : prog["sprak"],
                      "studieort"     : prog["ort"],
                      "vof"           : prog["vof"],
                      "programurl"    : programurl,
                      "programnamn"   : programnamn,
                  })


          # try:
          #   kurstid_id = c.fetchone()[0]

          #   for period in prog['periodtabell'].keys():
          #       blocks = prog['periodtabell'][period]
          #       c.execute('INSERT INTO period(kurstid_id, period) VALUES (?,?)',
          #                 (kurstid_id, period))
          #       for block in blocks:
          #           c.execute('INSERT INTO block(kurstid_id, period, block) VALUES (?,?,?)',
          #                     (kurstid_id, period, block))
          # except:
          #   pass


    except Exception as e:
          log.error("Error %s while processing %s",
                    e, filename)
          log.debug("traceback", exc_info=e)

except Exception as e:
    log.error("Error %s while processing %s",
              e, filename)
    log.debug("traceback", exc_info=e)

finally:
    conn.commit()
    conn.close()
    log.info("Insert finished")


