;;; -*- geiser-scheme-implementation: 'chicken -*-

;;; Description:

;; Builds an index of all courses or programs
;; Creates a directory with one file for each course/program, containing an URL to fetch the real data.

;;; Code:


(require-extension html-parser)
(require-extension sxpath)

(import (chicken string)
        (chicken process-context)
        (chicken file)
        (chicken format)
        (only (chicken file posix)
              file-permissions)
        matchable)

(define usage
  "Usage: ./parse input-file.html output-file")

(define-values (input-file output-file)
  (match (command-line-arguments)
    [() (print usage) (exit 1)]
    [(from to rest ...)
     (unless (null? rest)
       (format #t "WARNING: Arguments ~a ignored~%"))
     (values from to)]))

(define doc
  (call-with-input-file input-file html->sxml))

(define items
  ((sxpath '(// (div (@ class (equal? "study-guide-search-results")))
                section div))
   doc))

(with-output-to-file output-file
  (lambda ()
    (format #t "#!/bin/sh~%set -x
if [ $# -eq 0 ]; then
    echo \"Usage: $0 destdir\" > /dev/stderr
    exit 1
fi
DESTDIR=$1
")
    (map (lambda (item)
           (let ((link (apply string-append ((sxpath '(// a @ href "text()")) item)))
                 (name (apply string-append ((sxpath '(// h3 "text()")) item))))
             (format #t "curl -sLo \"$DESTDIR/~a.html\" \"~a\"~%"
                     (car (string-split name " "))
                     link)))
         items)))

(set! (file-permissions output-file) #o755)

