#!/bin/bash

set -x

mkdir -p /var/lib/studieinfo/stuff
mv $ARTDIR "/var/lib/studieinfo/stuff/"

# TODO atomic relink
rm /var/lib/studieinfo/{kurs,program,studieinfo.db}
ln -s stuff/$date/html/kurs /var/lib/studieinfo/kurs
ln -s stuff/$date/html/program /var/lib/studieinfo/program
ln -s stuff/$date/studieinfo.db /var/lib/studieinfo

