"""
This file should be run through the C preprocessor to get a "real" python file.
"""

from bs4 import BeautifulSoup
import json
import traceback
import sys
import os
import logging
from logger import setup_logging

setup_logging()
log = logging.getLogger("studieinfo.parse.course")

def try_it(f):
    res = None
    try:
        res = f()
    except Exception as e:
        log.error('%s\n%s', sys.argv[1], e)
        log.debug('traceback', exc_info=e)

    return res


#define FUCK(...) try_it(lambda : __VA_ARGS__)

def get_everything(soup):
    info_block = FUCK(soup.body.find("aside", {"class": "studyguide-aside"}).find("h3", text="Information").parent.find("div"))
    block2 = FUCK(soup.body.find("section", {"class": "studyguide-block"}))
    block3 = FUCK(soup.body.find("table", {"class": "table table-striped study-guide-table"}))
    block4 = soup.findAll("div", {"class" : "studyguide-block-full-width" })[0]

    def examination_row(x):
        kod, beskrivning, betygsskala, hp = x.find_all("td")
        return {
            'kod'         : kod.text,
            'beskrivning' : beskrivning.text,
            'betygsskala' : betygsskala.text,
            'hp'          : hp.text.replace(" hp","")
            }

    def period_block_grunka(p, b):
        """  period "1,2", block "3+4,2+4" """
        return { str(i).strip(): [x.strip() for x in j]
                for (i, j) in zip(p.split(','),
                                  [x.split("+") for x in b.split(",")]) }


    def programtabell_row(x):
        td = x.find_all("td")
        *_, programkod, antagningskod = td[1].find("a")['href'].split('/')
        return {
            'programkod'    : programkod,
            'antagningskod' : antagningskod,
            'programtermin' : FUCK(td[2].text.strip().split(" ")[0]),
            'termin'        : FUCK(td[2].text.strip()[3:-1]),
            'period'        : FUCK(td[3].text.strip()),
            'block'         : FUCK(td[4].text.strip()),
            'periodtabell'  : FUCK(period_block_grunka(x.find_all("td")[3].text.strip(), x.find_all("td")[4].text.strip())),
            'sprak'         : FUCK(td[5].text.strip()),
            'studieort'     : FUCK(td[6].text.strip()),
            'vof'           : FUCK(td[7].text.strip()),
            'programurl'    : FUCK(td[1].find("a")['href']),
            'programnamn'   : FUCK(td[1].find("a").text.strip()),
            }

    ret = {}

    try:
        ret["itemurl"] = itemurl = soup.find("head")['data-itemurl']
        *_, ret["kurskod"], tid = itemurl.split('/')
        ret["termin"], ret["anno"], *_ = tid.split('-')
    except:
        log.warning("Error in itemurl stuff")


    try:
        kursnamn_sv, hp, kursnamn_en, *_ = info_block.find_all("h4")
        ret.update(kursnamn_sv=kursnamn_sv.text,
                   kursnamn_en=kursnamn_en.text,
                   hp=hp.text.replace(" hp", ""))
    except:
        log.warning("Error in kursnamn stuff")


    return {

        **ret,

        'fakultet'           : FUCK(info_block.find("span", text="Fakultet").next_sibling.next_sibling),
        'galler_fran'        : FUCK(info_block.find("span", text="Gäller från").next_sibling.next_sibling),
        'faststalld_av'      : FUCK(info_block.find("span", text="Fastställd av").next_sibling.next_sibling),
        'faststallandedatum' : FUCK(info_block.find("span", text="Fastställandedatum").next_sibling.next_sibling),
        'revideringsdatum'   : FUCK(info_block.find("span", text="Revideringsdatum").next_sibling.next_sibling),
        'diarienummer'       : FUCK(info_block.find("span", text="Diarienummer").next_sibling.next_sibling),

        'kurstyp'                        : FUCK(block4.find("h3", text="Kurstyp").next_sibling.strip()),
        'huvudomrade'                    : FUCK(block4.find("h3", text="Huvudområde").next_sibling.strip()),
        'utbildningsniva'                : FUCK(block4.find("h3", text="Nivå").next_sibling.strip()),
        'fordjupningsniva'               : FUCK(block2.find("h3", text="Fördjupningsnivå").next_sibling.strip()),
        'forkunskapskrav'                : FUCK(block2.find("h3", text="Förkunskapskrav").parent.prettify()),
        'rekommenderade_forkunskaper'    : FUCK(block2.find("h3", text="Rekommenderade förkunskaper").parent.prettify()),
        'larandemal'                     : FUCK(block2.find("h3", text="Lärandemål").parent.prettify()),
        'kursinnehall'                   : FUCK(block2.find("h3", text="Kursinnehåll").parent.prettify()),
        'undervisnings_och_arbetsformer' : FUCK(block2.find("h3", text="Undervisnings- och arbetsformer").parent.prettify()),
        'betygsskala'                    : FUCK(block2.find("h3", text="Betygsskala").next_sibling.strip()),
        'ovrig_information'              : FUCK(block2.find("h3", text="Övrig information").parent.prettify()),
        'amnesomrade'                    : FUCK(block2.find("h3", text="Ämnesområde").next_sibling.strip()),
        'institution'                    : FUCK(block2.find("h3", text="Institution").next_sibling.strip()),
        'studierektor'                   : FUCK(block4.find("h3", text="Studierektor eller motsvarande").next_sibling.strip()),
        'examinator'                     : FUCK(block4.find("h3", text="Examinator").next_sibling.strip()),
        'lankar'                         : FUCK(block2.find("h3", text="Kurshemsida och andra länkar").parent.prettify()),
        'undervisningstid'               : FUCK(block2.find("h3", text="Undervisningstid").parent.text.split('\n')[2].strip()),
        'kurslitteratur'                 : FUCK(block2.find("h3", text="Kurslitteratur").parent.prettify()),

        'kurstidertabell'          : FUCK(list(map(programtabell_row, block3.find_all("tr")[1:]))),
        'examinationsmomenttabell' : FUCK(list(map(examination_row, block2.find("h3", text="Examination").parent.find("table").find_all("tr")))),
        'annos' : FUCK(list(map(lambda x: x['value'].split('/')[-1], soup.find("div", {'class':"study_guide_navigation"}).findAll("option")))),
        'senaste' : FUCK(0 if ':' in sys.argv[1] else 1)
        }

if __name__ == "__main__":
    filename, out_filename, *_ = sys.argv[1:]
    try:
        j = ""
        with open(filename, 'r', errors='replace') as f:
            res = get_everything(BeautifulSoup(f.read(), "html5lib"))
            from datetime import datetime
            t = os.stat(f.fileno()).st_mtime
            res['stat_mtime'] = datetime.utcfromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')
            j = json.dumps(res)

        with open(out_filename, 'w') as f:
            f.write(j)
            

    except Exception as e:
        log.error('%s\n%s', sys.argv[1], e)
        log.debug('traceback', exc_info=e)
