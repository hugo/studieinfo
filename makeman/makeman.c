#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/stat.h> // mkdir
#include <linux/limits.h> // PATH_MAX

#include <sqlite3.h>
#include <gumbo.h>

#define KURSID               0
#define KURSKOD              1
#define KURSNAMN_SV          2
#define KURSNAMN_EN          3
#define URL                  4
#define FORDJUPNINGSNIVA     5
#define HUVUDOMRADE          6
#define DIARIENUMMER         7
#define HP                   8
#define KURSTYP              9
#define FAKULTET             10
#define GALLER_FRAN          11
#define FASTSTALLD_AV        12
#define FASTSTALLANDEDATUM   13
#define REVIDERINGSDATUM     14
#define BETYGSSKALA          15
#define AMNESOMRADE          16
#define INSTITUTION          17
#define STUDIEREKTOR         18
#define EXAMINATOR           19

#define KURSINEHALL                   20
#define LARANNDEMAL                   21
#define UNDERVISNINGS_OCH_ARBETSFORMER 22
#define FORKUNSKAPSKRAV               23
#define OVRIG_INFORMATION             24
#define LANKAR                        25
#define UNDERVISNINGSTID              26
#define KURSLITTERATUR                27
#define STAT_MTIME                    28
#define REKOMMENDERADE_FORKUNSKAPER   29

/* command line arguments */
char out_path[PATH_MAX] = "man7";
char db_path[PATH_MAX] = "studieinfo.db";

int bulstyle = 0;

#define STR(s) #s

/* Whitespace predicate */
#define WS_P(c) (c == ' ' || c == '\t' || c == '\n' || c == '\r')

int print_trimmed(FILE* f, const char* str) {
	int len = strlen(str);
	if (len == 0) return 1;
	char* og_c = calloc(len + 1, 1);
	char* c = og_c;
	memcpy(c, str, len);

	while (WS_P(*c)) c++;

	int end = strlen(c);
	char* q = c + end; // could be replaced by og_c + len
	while (WS_P(*q) || *q == '\0') q--;
	*(q + 1) = '\0';

	fputs(c, f);
	free (og_c);
	return 0;
}

int put_trimmed(FILE* f, const char* str) {
	print_trimmed(f, str);
	fputs("\n", f);
	return 0;
}

#define FOR_CHILDREN(EL) do { \
	GumboVector v = (EL)->children; \
	for (unsigned int i = 0; i < v.length; i++) { \
		html_recursive(f, v.data[i]); \
	} \
} while (0)



#define DIRECT_CHILDTEXT(EL) do { \
	GumboVector v = (EL)->children; \
	GumboNode* ch; \
	for (int i = 0; i < v.length; i++) { \
		ch = v.data[i]; \
		if (ch->type == GUMBO_NODE_TEXT) { \
			put_trimmed(f, ch->v.text.text); \
		} \
	} \
} while (0)

int html_recursive (FILE* f, GumboNode* n) {
	GumboElement* el;
	switch (n->type) {
		case GUMBO_NODE_ELEMENT:
			el = &n->v.element;
			switch (el->tag) {
				/* These are all kind of div's */
				case GUMBO_TAG_ARTICLE:
				case GUMBO_TAG_SECTION:
				case GUMBO_TAG_NAV:
				case GUMBO_TAG_ASIDE:
				case GUMBO_TAG_HGROUP:
				case GUMBO_TAG_HEADER:
				case GUMBO_TAG_FOOTER:
				case GUMBO_TAG_MAIN:
				case GUMBO_TAG_DIV:
					FOR_CHILDREN(el);

					break;
				case GUMBO_TAG_H1:
				case GUMBO_TAG_H2:
					fprintf(f, ".SH ");
					DIRECT_CHILDTEXT(el);
					break;
					
				case GUMBO_TAG_H3:
					fprintf(f, ".SS ");
					DIRECT_CHILDTEXT(el);
					break;

				case GUMBO_TAG_H4:
				case GUMBO_TAG_H5:
				case GUMBO_TAG_H6:
					fputs(".PP\n", f);
					fprintf(f, ".B ");
					DIRECT_CHILDTEXT(el);
					break;

				case GUMBO_TAG_P:
					fputs(".PP\n", f);
					FOR_CHILDREN(el);
					break;

				case GUMBO_TAG_HR:
					fputs("l\n", f);
					break;

				case GUMBO_TAG_BLOCKQUOTE:
					fputs(".QP\n", f);
					FOR_CHILDREN(el);
					break;

				// case GUMBO_TAG_S:
				// case GUMBO_TAG_DEL:

				case GUMBO_TAG_Q:
					fputs("\\(lq", f);
					DIRECT_CHILDTEXT(el);
					fputs("\\(rq", f);
					break;

				case GUMBO_TAG_OL:
					bulstyle = 1;
					fputs(".nr step 1 1\n", f);
					FOR_CHILDREN(el);
					break;

				case GUMBO_TAG_A:
				case GUMBO_TAG_UL:
					bulstyle = 0;
					FOR_CHILDREN(el);
					break;

				case GUMBO_TAG_LI:
					fprintf(f, ".IP %s\n", bulstyle == 1 ? "\n+[step]" : "\\[bu]");
					FOR_CHILDREN(el);
					break;


				// TODO does this work?
				case GUMBO_TAG_SUP:
					fputs("\\*{\n", f);
					FOR_CHILDREN(el);
					fputs("\\*}\n", f);
					break;

				case GUMBO_TAG_SUB:
					fputs(".SM\n", f);
					FOR_CHILDREN(el);
					fputs(".NL\n", f);

				case GUMBO_TAG_PRE:
				case GUMBO_TAG_DFN:
				case GUMBO_TAG_ABBR:
				case GUMBO_TAG_DATA:
				case GUMBO_TAG_VAR:
				case GUMBO_TAG_SAMP:
				case GUMBO_TAG_KBD:
				case GUMBO_TAG_CODE:
					fprintf(f, ".BX \"");
					DIRECT_CHILDTEXT(el);
					fputs("\"\n", f);
					break;

				case GUMBO_TAG_EM:
				case GUMBO_TAG_I:
					fputs(".I\n", f);
					FOR_CHILDREN(el);
					fputs(".R\n", f);
					break;

				case GUMBO_TAG_STRONG:
				case GUMBO_TAG_B:
					fputs(".B\n", f);
					FOR_CHILDREN(el);
					fputs(".R\n", f);
					break;

				case GUMBO_TAG_U:
					fprintf(f, ".UL \"");
					DIRECT_CHILDTEXT(el);
					fputs("\"\n", f);
					break;

				case GUMBO_TAG_BR:
					fputs(".LP\n", f);
					break;

				default:
					FOR_CHILDREN(el);

			}

			break;

		case GUMBO_NODE_TEXT:
			put_trimmed(f, n->v.text.text);
			break;

		default:
			;
			// FOR_CHILDREN(el);
	}
	return 0;
}

int html_to_groff (FILE* f, const char* str) {
	if (str == NULL) return 1;
	GumboOutput* output = gumbo_parse(str);
	// fputs(str, stderr);
	html_recursive(f, output->root);
	gumbo_destroy_output(&kGumboDefaultOptions, output);
	return 0;
}
#define TSTR "T{\n%s\nT}"

sqlite3* db;

int kurstid_callback(void* data, int cols, char** t, char** column_names) {
	FILE* f = (FILE*) data;

	if (t[0] == NULL) return 0;

	fprintf(f, TSTR "@" TSTR "@" TSTR "@" TSTR "@" TSTR "@" TSTR "@" TSTR "@" TSTR "\n",
			t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7]);
	return 0;
}

int exam_callback(void* data, int cols, char** t, char** column_names) {
	FILE* f = (FILE*) data;

	fprintf(f, TSTR "@" TSTR "@" TSTR "@" TSTR "\n",
			t[0], t[1], t[2], t[3]);

	return 0;
}

int upcase(char* str) {
	for (char* c = str; *c != '\0'; c++) {
		if ('a' <= *c && *c <= 'z') {
			*c &= ~0x20;
		}
	}
	return 0;
}

int callback (void* data, int cols, char** t, char** column_names) {

	upcase(t[KURSKOD]);

	char buf[0x200] = { 0 };
	strcat(buf, out_path);
	strcat(buf, "/");
	strcat(buf, t[KURSKOD]);
	strcat(buf, ".7studieinfo");
	FILE* f = fopen(buf, "w");
	buf[0] = '\0';

	printf("Handling %s\n", t[KURSKOD]);

	fprintf(f, ".TH Studieinfo %s\n", t[KURSKOD]);

	fputs(".SH NAME\n", f);
	fprintf(f, "%s \\- %s\n", t[KURSKOD], t[KURSNAMN_SV]);

	fputs(".SH SAMMANFATTNING\n", f);
	fputs(".TS\n", f);
	fputs("tab(@);\n", f);
	fputs("r l.\n", f);
	fprintf(f, TSTR "@" TSTR "\n_\n", ".B Vad", ".B Info");
	fprintf(f, TSTR "@" TSTR "\n", ".B Kursnamn Sv", t[KURSNAMN_SV]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Kursnamn En", t[KURSNAMN_EN]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Fördjupningsnivå", t[FORDJUPNINGSNIVA]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Huvudområde", t[HUVUDOMRADE]);
	fprintf(f, TSTR "@" TSTR "\n", ".B HP", t[HP]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Kurstyp", t[KURSTYP]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Fakultet", t[FAKULTET]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Gäller från", t[GALLER_FRAN]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Fasstställd av", t[FASTSTALLD_AV]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Fastställandedatum", t[FASTSTALLANDEDATUM]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Revideringsdatum", t[REVIDERINGSDATUM]); // quite often NULL
	fprintf(f, TSTR "@" TSTR "\n", ".B Ämnesområde", t[AMNESOMRADE]); // quite often NULL
	fprintf(f, TSTR "@" TSTR "\n", ".B Institution", t[INSTITUTION]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Studierektor", t[STUDIEREKTOR]);
	fprintf(f, TSTR "@" TSTR "\n", ".B Examinator", t[EXAMINATOR]);

	fputs(".TE\n", f);

	fputs(".SH EXAMINATIONSMOMENT\n", f);
	fputs(".TS\n", f);
	fputs("tab(@);\n", f);
	fputs("l l l l.\n", f);
	fprintf(f, TSTR "@" TSTR "@" TSTR "@" TSTR "\n_\n",
			".B Kod", ".B HP", ".B Beskrivning", ".B Betygsskala");

	strcat(buf, "SELECT kod, hp, beskrivning, betygsskala FROM examinationsmoment WHERE kurs_id =");
	strcat(buf, t[0]);
	sqlite3_exec(db, buf, exam_callback, f, NULL);
	fputs(".TE\n", f);

	buf[0] = '\0';

	fputs(".SH KURSTIDER\n", f);

	fputs(".TS\n", f);
	fputs("tab(@);\n", f);
	fputs("l l l l l l l l.\n", f);
	fprintf(f, TSTR "@" TSTR "@" TSTR "@" TSTR "@"
			   TSTR "@" TSTR "@" TSTR "@" TSTR "\n_\n",
			   ".B Programkod", ".B Pt", ".B Termin", ".B P*",
			   ".B B*", ".B Språk", ".B Studieort", ".B VOF*");

	strcat(buf, "SELECT programkod, programtermin, termin, period, block, sprak, studieort, vof FROM kurstider WHERE kurs_id = ");
	strcat(buf, t[0]);
	sqlite3_exec(db,
			buf,
			kurstid_callback,
			f, NULL);
	buf[0] = '\0';

		

	fputs(".TE\n", f);

	fputs(".SH INFORMATION OM KURSEN\n", f);

	fputs(".SS Kursinehåll\n", f);
	html_to_groff(f, t[KURSINEHALL                   ] );
	fputs(".SS Lärandemål\n", f);
	html_to_groff(f, t[LARANNDEMAL                   ] );
	fputs(".SS Undervisnings- och arbetsformer\n", f);
	html_to_groff(f, t[UNDERVISNINGS_OCH_ARBETSFORMER] );
	fputs(".SS Rekommenderade förkunskaper\n", f);
	html_to_groff(f, t[REKOMMENDERADE_FORKUNSKAPER   ] );
	fputs(".SS Förkonskapskrav\n", f);
	html_to_groff(f, t[FORKUNSKAPSKRAV               ] );
	fputs(".SS Övrig information\n", f);
	html_to_groff(f, t[OVRIG_INFORMATION             ] );
	fputs(".SS Länkar\n", f);
	html_to_groff(f, t[LANKAR                        ] );
	fputs(".SS Undervisningstid\n", f);
	html_to_groff(f, t[UNDERVISNINGSTID              ] );
	fputs(".SS Kurslitteratur\n", f);
	html_to_groff(f, t[KURSLITTERATUR                ] );

	fputs(
			".SH OM SIDAN\n"
			"Den här sidan är en del av Lysators studiehandbok. "
			"Ordentlig dokumentation och länk till alla sidor finns på\n"

			".IP\nhttps://studieinfo.lysator.liu.se\n.PP\n"

			"Källkod med mera finns på\n"

			".IP\nhttps://git.lysator.liu.se/hugo/studieinfo\n.PP\n"

			"Risken för fel på man-sidorna är aningen högre än densamme "
			"på HTML sidorna, då de här bygger på en egenskriven "
			"HTML->troff konverterare."
			,
			f
		 );

	time_t now = time(NULL);
	struct tm* time = localtime(&now);
	strftime(buf, 0x100, "%Y-%m-%d %T", time);

	fputs("\n.TS\n", f);
	fputs("tab(@);\n", f);
	fputs("r l.\n", f);
		fprintf(f, TSTR "@" TSTR "\n",
				"Sidan utgenererad:", buf);

		fprintf(f, TSTR "@" TSTR "\n",
				"Datan hämtad:", t[STAT_MTIME]);
	fputs(".TE\n", f);

	fclose(f);

	return 0;
}

enum cmd_opts {
	ERROR, DATABASE, OUTDIR, HELP,
};

enum cmd_opts parse_long_opt (char* str) {
#define CASE(s, target) if (strcmp (str, (s)) == 0) return (target);
	CASE("database", DATABASE);
	CASE("outdir", OUTDIR);
	return ERROR;
#undef CASE
}

enum cmd_opts parse_short_opt (char c) {
	switch (c) {
		case 'd': return DATABASE;
		case 'o': return OUTDIR;
		default: return ERROR;
	}
}

int main(int argc, char* argv[argc]) {

	/* arg parse */
	char* s;
	for (int i = 1; i < argc; i++) {
		s = argv[i];
		if (s[0] == 0) continue;
		if (s[0] == '-') {
			++s;
			enum cmd_opts opt;
			if (s[0] == '-') {
				opt = parse_long_opt(++s);
			} else {
				opt = parse_short_opt(s[0]);
			}

			switch (opt) {
				case DATABASE:
					strcpy(db_path, argv[++i]);
					break;
				case OUTDIR:
					strcpy(out_path, argv[++i]);
					break;
				case HELP:
					puts("**shrug**\n");
					exit(0);
				case ERROR:
					printf("Unknown option [%s]\n", s);
					exit(1);
			}
		}
	}

	mkdir(out_path, 0755);

	int ret = sqlite3_open(db_path, &db);

	if (ret != 0) {
		printf("Error opening: ");
		puts(sqlite3_errstr(ret));
		goto cleanup;
	}

	ret = sqlite3_exec(db,
			"SELECT "
			"id"
			", kurskod "
			", kursnamn_sv "
			", kursnamn_en "
			", url "
			", fordjupningsniva "
			", huvudomrade "
			", diarienummer "
			", hp "
			", kurstyp "
			", fakultet "
			", galler_fran "
			", faststalld_av "
			", faststallandedatum "
			", revideringsdatum "
			", betygsskala "
			", amnesomrade "
			", institution "
			", studierektor "
			", examinator "
			", kursinnehall                   "
			", larandemal                    "
			", undervisnings_och_arbetsformer "
			", forkunskapskrav               "
			", ovrig_information             "
			", lankar                        "
			", undervisningstid              "
			", kurslitteratur                "
			", stat_mtime "
			", rekommenderade_forkunskaper "
			"FROM kurs "
			,
			callback,
			NULL, NULL);

	if (ret != 0) {
		printf("Bad return from query: ");
		puts(sqlite3_errstr(ret));
		goto cleanup;
	}

cleanup:
	sqlite3_close(db);

	return ret;
}
