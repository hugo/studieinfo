#!/usr/bin/env python3

"""
Insert json blobs produced by parse-program into the database.
"""

import sqlite3
import json
import sys
import os
import argparse
import logging
from logger import setup_logging

# arg parser
# ==========

parser = argparse.ArgumentParser(description='Insert data into the database')
parser.add_argument('--database',  default='studieinfo.db', help='sqlite3 file')
parser.add_argument('--indir',  default='program-json', help='Directory of json files')

args = parser.parse_args()
db = args.database
indir = args.indir

setup_logging()
log = logging.getLogger("studieinfo.insert.program")

log.info('Starting insert')

# ====================

conn = sqlite3.connect(db)
c = conn.cursor()
c.execute('PRAGMA FOREIGN_KEYS = ON')



column_names = [
        "url",
        "programkod",
        "programnamn_sv",
        "programnamn_en",
        "antagningskod",
        "undervisningssprak",
        "studieort",
        "examensbenamning",
        "studietakt",
        "utbildningsplan",
        "generella_bestammelser",
        "antagningstillfalle",
        "programplan_json",
        "stat_mtime",
        "senaste",
        ]

try:
    for filename in os.listdir(indir):
        with open(indir+'/'+filename, 'r') as f:
            data = json.loads(f.read())

        row = tuple(data[x] for x in column_names)

        # if not (data['kurskod'] and data['galler_fran']): 
        #     exit (0)

        try:
            c.execute('INSERT INTO program ' + str(tuple(column_names)) + ' VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', row)
            c.execute("SELECT id FROM program WHERE " + "".join([x + "=? AND " for x in column_names])[:-4], row)
            program_id = c.fetchone()[0]

            for termin in data['programplan']:
                c.execute('INSERT INTO programplan_termin (program_id, termin) VALUES (?,?)', (program_id, termin['termin']))
                c.execute('SELECT last_insert_rowid()')
                termin_id = c.fetchone()[0]

                for specialisering in termin['specialiseringar']:
                    c.execute('INSERT INTO programplan_specialisering (termin_id, specialisering) VALUES (?,?)',
                            (termin_id, specialisering['specialisering']))
                    c.execute('SELECT last_insert_rowid()')
                    specialisering_id = c.fetchone()[0]

                    for period in specialisering['perioder']:
                        c.execute('INSERT INTO programplan_period (specialisering_id, period) VALUES (?,?)',
                                (specialisering_id, period['period']))
                        c.execute('SELECT last_insert_rowid()')
                        period_id = c.fetchone()[0]

                        for kurs in period['kurser']:
                            c.execute('INSERT INTO programplan_kurs (period_id, kurskod, kursnamn, hp, niva, block, vof) VALUES (?,?,?,?,?,?,?)', 
                                    (period_id, kurs['kurskod'], kurs['kursnamn'], kurs['hp'], kurs['niva'], kurs['block'], kurs['vof']))

        except Exception as e:
             log.warning('%s\n%s', filename, e)
             log.debug('traceback', exc_info=e)

except Exception as e:
      log.error('%s\n%s', filename, e)
      log.error('traceback', exc_info=e)

finally:
      conn.commit()
      conn.close()

log.info('insert complete')
