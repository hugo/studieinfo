Name:            studieinfo
Version:         1.5.2
Release:         0
Summary:         Studieinfo
License:         MIT
URL:             https://git.lysator.liu.se/hugo/studieinfo
Source:          %{url}/-/archive/master/studieinfo-master.tar.gz
Prefix:          %{_prefix}
Packager:        hugo <hugo@lysator.liu.se>
BuildRoot:       %{tmppath}/%{name}-root
#BuildRequires:   sqlite-devel gumbo-parser chicken-devel
Requires:        python3 sqlite dpkg chicken
# Requires:        pyyaml
# python36-beautifulsoup4 

%description
studieinfo

%prep
%setup

%pre
getent passwd studieinfo >/dev/null \
    || useradd --system -d /usr/lib/studieinfo -s /sbin/nologin studieinfo


%build
make -j

%install
make install DESTDIR=$RPM_BUILD_ROOT

%files
%config /etc/studieinfo/studieinfo.ini
%config /etc/studieinfo/logging.yaml
# %ghost /var/lib/studieinfo/
/usr/lib/systemd/system/studieinfo-*
/usr/lib/studieinfo/
/usr/share/studieinfo/
